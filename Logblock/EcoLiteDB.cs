﻿using System;
using System.IO;
using Asphalt.Api.Event.PlayerEvents;
using Eco.Gameplay.Players;
using LiteDB;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Asphalt.Events.WorldObjectEvent;

namespace Logblock.dataBase
{
    public static class EcoLiteDB
    {
        private static LiteCollection<dataBaseRecord.LoggedBlockPlacement> Collection;
        private static LiteDatabase Database;
        public static string timeZone = TimeZoneNames.TZNames.GetAbbreviationsForTimeZone(TimeZone.CurrentTimeZone.StandardName, CultureInfo.CurrentCulture.Name).Standard;

        public static void LogToDatabase(PlayerPlaceEvent e)
        {
            CheckDB();
            Player player = e.Player;
            string playerName = player.ToString();
            playerName = playerName.Split('<')[4].Split('>')[1];
            //Find user in userManager
            User user = UserManager.FindUserByName(playerName);
            var record = new dataBaseRecord.LoggedBlockPlacement
            {
                PlayerId = user.Id,
                PlayerName = playerName,
                action = "Placed",
                ItemName = e.Item.DisplayName,
                X = e.Position.X,
                Y = e.Position.Y,
                Z = e.Position.Z,
                Time = DateTime.Now,
                TimeZone = timeZone
            };
            Collection.Insert(record);
        }
        public static void LogToDatabase(PlayerPickUpEvent e)
        {

            CheckDB();
            Player player = e.Player;
            string playerName = player.ToString();
            playerName = playerName.Split('<')[4].Split('>')[1];
            //Find user in userManager
            User user = UserManager.FindUserByName(playerName);
            var record = new dataBaseRecord.LoggedBlockPlacement
            {
                PlayerId = user.Id,
                PlayerName = playerName,
                action = "Picked Up",
                ItemName = e.PickedUpItem.DisplayName,
                X = e.Position.X,
                Y = e.Position.Y,
                Z = e.Position.Z,
                Time = DateTime.Now,
                TimeZone = timeZone
            };
            Collection.Insert(record);
        }
        public static void LogToDatabase(TreeFellEvent e)
        {

            CheckDB();
            User user = UserManager.FindUserByName(e.Killer.ToString());
            var record = new dataBaseRecord.LoggedBlockPlacement
            {
                PlayerId = user.Id,
                PlayerName = user.Name,
                action = "Chopped tree",
                ItemName = e.TreeEntity.DisplayName,
                X = e.TreeEntity.Position.WorldPosition3i.x,
                Y = e.TreeEntity.Position.WorldPosition3i.y,
                Z = e.TreeEntity.Position.WorldPosition3i.z,
                Time = DateTime.Now,
                TimeZone = timeZone
            };
            Collection.Insert(record);
        }

        public static List<dataBaseRecord.LoggedBlockPlacement> QueryBlockAt(int X, int Y, int Z)
        {
            LiteCollection<dataBaseRecord.LoggedBlockPlacement> record = Database.GetCollection<dataBaseRecord.LoggedBlockPlacement>();
            IEnumerable<dataBaseRecord.LoggedBlockPlacement> results = record.Find(x => x.X.Equals(X) && x.Y.Equals(Y) && x.Z.Equals(Z));
            if (results.Count() > 0)
            {
                return results.ToList();
            }
            else
            {
                return null;
            }
        }

        public static void CheckDB()
        {
            if (Database != null && Collection != null)
            {
                return;
            }
            Console.WriteLine("Db failed, initializing");
            InitDB();
        }

        public static void InitDB(string dbPath = null)
        {
            if (dbPath == null)
            {
                dbPath = Path.Combine(Directory.GetCurrentDirectory(), @"Storage\LogBlock.db");
            }
            Console.WriteLine($"Opening database at [{dbPath}] with prunedays of [{LogblockMod.config.daysTillPrune}]");
            if (!File.Exists(dbPath))
            {
                File.Create(dbPath);
            }
            try
            {
                Database = new LiteDatabase(dbPath);
                Collection = Database.GetCollection<dataBaseRecord.LoggedBlockPlacement>();
                //start pruning thread
                if (LogblockMod.config.daysTillPrune > 0)
                {
                    Thread pruneThread = new Thread(() => Prune());
                    pruneThread.Start();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void Prune()
        {
            Thread.Sleep(500); //Prevent Access Errors
            while (true)
            {
                Collection.Delete(x => (DateTime.Now - x.Time).Days > LogblockMod.config.daysTillPrune);
                //Check every 30 minutes
                Thread.Sleep(30 * 6000);
            }
        }
    }
}