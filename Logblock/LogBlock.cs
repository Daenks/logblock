﻿using System;
using Asphalt;
using Asphalt.Api.Event;
using Asphalt.Api.Event.PlayerEvents;
using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Gameplay.Players;
using Logblock.Config;
using EcoColorLib;
using Eco.Mods.TechTree;
using NIDMod;
using System.IO;
using Newtonsoft.Json;
using Asphalt.Events.WorldObjectEvent;

namespace Logblock
{
    [AsphaltPlugin]
    class LogblockMod : IModKitPlugin, IInitializablePlugin
    {
        public static String prefix = "LogBlock: ";
        public static String coloredPrefix = ChatFormat.Green.Value + ChatFormat.Bold.Value + prefix + ChatFormat.Clear.Value;
        public static LogBlockConfig config;
        public static Moderator[] Moderators;
        public static int savingWorld;

        public string GetStatus()
        {
            return "";
        }

        public void Initialize(TimedTask timer)
        {
            Console.WriteLine("Starting logBlock");
            config = new LogBlockConfig("LogBlock", "config");
            if (config.exist())
            {
                config = config.reload<LogBlockConfig>();
            }
            else
            {
                config.save();
            }
            dataBase.EcoLiteDB.InitDB(dbPath: null);
            if (config.LogBlockPlacement)
            {
                EventManager.RegisterListener(new Listeners.Place());
            }
            if (config.LogBlockPickup)
            {
                EventManager.RegisterListener(new Listeners.Pickup());
            }
            EventManager.RegisterListener(new Listeners.TreeFell());
            //Load nid Moderators
            if (config.UseNidPermissions)
            {
                Nid nid = JsonConvert.DeserializeObject<Nid>(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), config.PathToNidSettings)));
                Moderators = nid.ModeratorsModule.Moderators;
            }
        }

        public static void LogToConsole(PlayerPlaceEvent e)
        {
            Eco.Shared.Math.Vector3i pos = e.Position;
            Player player = e.Player;
            Eco.Gameplay.Items.BlockItem item = e.Item;
            string playerName = player.ToString();
            Console.WriteLine($"Player [{playerName.Split('<')[4].Split('>')[1]}] placed [{item.DisplayName}] at [{pos.X},{pos.Y},{pos.Z}]");
        }
        public static void LogToConsole(PlayerPickUpEvent e)
        {
            Eco.Shared.Math.Vector3i pos = e.Position;
            Player player = e.Player;
            var item = e.PickedUpItem;
            string playerName = player.ToString();
            Console.WriteLine($"Player [{playerName.Split('<')[4].Split('>')[1]}] picked up [{item.DisplayName}] at [{pos.X},{pos.Y},{pos.Z}]");
        }
        public static void LogToConsole(TreeFellEvent e)
        {
            Eco.Shared.Math.Vector3 pos = e.TreeEntity.Position;
            //Player player = e.Context.Player;
            Console.WriteLine($"Player [{e.Killer}] chopped a tree at [{pos.X},{pos.Y},{pos.Z}]");
        }
    }
}