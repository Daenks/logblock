﻿using System;
using Asphalt.Api.Event;
using Asphalt.Api.Event.PlayerEvents;

namespace Logblock.Listeners
{
    class Place
    {
        [EventHandler(EventPriority.Normal, RunIfEventCancelled = false)]   //default values
        public void PlayerPlace(PlayerPlaceEvent evt)
        {
            if (LogblockMod.config.LogToConsole)
            {
                LogblockMod.LogToConsole(evt);
            }
            dataBase.EcoLiteDB.LogToDatabase(evt);
        }
    }
}
