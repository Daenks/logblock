﻿using System;
using Asphalt.Api.Event;
using Asphalt.Api.Event.PlayerEvents;

namespace Logblock.Listeners
{
    class Pickup
    { 
        //TODO causes AMBIGIOUS type execption
        [EventHandler(EventPriority.Normal, RunIfEventCancelled = false)]
        public void PlayerPickUp(PlayerPickUpEvent PIevt)
        {
            if (LogblockMod.config.LogToConsole)
            {
                LogblockMod.LogToConsole(PIevt);
            }
            dataBase.EcoLiteDB.LogToDatabase(PIevt);
        }
    }
}
