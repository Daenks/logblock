﻿using System;
using Asphalt.Api.Event;
using Asphalt.Api.Event.PlayerEvents;
using Asphalt.Events.WorldObjectEvent;
using Eco.Simulation.Agents;

namespace Logblock.Listeners
{
    class TreeFell
    {
        [EventHandler(EventPriority.Normal, RunIfEventCancelled = false)]   //default values
        public void PlayerTreeFell(TreeFellEvent evt)
        {
            if (LogblockMod.config.LogToConsole)
            {
                LogblockMod.LogToConsole(evt);
            }
            dataBase.EcoLiteDB.LogToDatabase(evt);
        }
    }
}
