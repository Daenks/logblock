﻿using Eco.Core.Controller;
using Eco.Core.Plugins;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
//using Eco.ModKit.Internal;
using Eco.Mods;
using Eco.Shared.Localization;
using Eco.Shared.Utils;
using Eco.Shared.Math;
using NIDMod;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Logblock.Commands
{
    public class UserCommands : IChatCommandHandler
    {
        [ChatCommand("lb-QueryBlock", "checks who placed the block you are standing on", ChatAuthorizationLevel.User)]
        //User is the calling user
        public static void QueryBlock(User user)
        {
            if (LogblockMod.config.UseNidPermissions)
            {
                foreach (Moderator mod in LogblockMod.Moderators)
                {
                    if (mod.Identifier == user.SteamId || mod.Identifier == user.SlgId || user.IsAdmin)
                    {
                        //Check if user is a moderator in NID
                        List<dataBaseRecord.LoggedBlockPlacement> result = dataBase.EcoLiteDB.QueryBlockAt(user.Position.WorldPosition3i.x, user.Position.WorldPosition3i.y - 1, user.Position.WorldPosition3i.z);
                        if (result != null)
                        {
                            foreach (var item in result)
                            {
                                LocStringBuilder locStringBuilder = new LocStringBuilder();
                                locStringBuilder.Append($"Block {item.ItemName} was {item.action} here by {item.PlayerName} ({item.PlayerId}) at {item.Time} {item.TimeZone}");
                                user.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                            }
                        }
                        else
                        {
                            LocStringBuilder locStringBuilder = new LocStringBuilder();
                            locStringBuilder.Append("This block has not been placed");
                            user.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                        }
                        return;
                    }
                }
            }
        }
        [ChatCommand("lb-save", "checks who placed the block you are standing on", ChatAuthorizationLevel.User)]
        //User is the calling user
        public static async void Save(User user)
        {
            if (Interlocked.CompareExchange(ref LogblockMod.savingWorld, 1, 0) != 0)
            {
                user.Player.SendTemporaryError(Localizer.Do($"Somebody else is already saving the world. No need your help. Sorry"));
                return;
            }

            user.Player.SendTemporaryMessage(Localizer.Do($"Saving the world...."));
            await Task.Run(() =>
            {
                try
                {
                    var time = TickTimeUtil.TimeSubprocess(StorageManager.SaveAndFlush);
                    ChatManager.ServerMessageToPlayer(Localizer.Do($"You saved the world! It only took {time / 1000:N1} seconds for you!"), user);
                }
                finally
                {
                    Interlocked.Exchange(ref LogblockMod.savingWorld, 0);
                }
            });
        }
        [ChatCommand("lb-QueryTargetXYZ", "checks who placed the block at the coordinates", ChatAuthorizationLevel.User)]
        public static void QueryTargetXYZ(User user, int? X = null, int? Y = null, int? Z = null)
        {
            if (LogblockMod.config.UseNidPermissions)
            {
                foreach (Moderator mod in LogblockMod.Moderators)
                {
                    if (mod.Identifier == user.SteamId || mod.Identifier == user.SlgId || user.IsAdmin)
                    {
                        if (X == null)
                        {
                            if (user.FacingDir == Direction.Forward)
                            {
                                X = user.Position.WorldPosition3i.x + 1;
                            }
                            else if (user.FacingDir == Direction.Back)
                            {
                                X = user.Position.WorldPosition3i.x - 1;
                            }
                            else 
                            {
                                X = user.Position.WorldPosition3i.x;
                            }
                        }
                        if (Y == null)
                        {
                            if (user.FacingDir == Direction.Up)
                            {
                                Y = user.Position.WorldPosition3i.y + 1;
                            }
                            else if (user.FacingDir == Direction.Down)
                            {
                                Y = user.Position.WorldPosition3i.y - 1;
                            }
                            else
                            {
                                Y = user.Position.WorldPosition3i.y;
                            }
                        }
                        if (Z == null)
                        {
                            if (user.FacingDir == Direction.Right)
                            {
                                Z = user.Position.WorldPosition3i.z + 1;
                            }
                            else if (user.FacingDir == Direction.Left)
                            {
                                Z = user.Position.WorldPosition3i.z - 1;
                            }
                            else
                            {
                                Z = user.Position.WorldPosition3i.z;
                            }
                        }
                        List<dataBaseRecord.LoggedBlockPlacement> result = dataBase.EcoLiteDB.QueryBlockAt((int)X, (int)Y, (int)Z);
                        if (result != null)
                        {
                            foreach (var item in result)
                            {
                                LocStringBuilder locStringBuilder = new LocStringBuilder();
                                locStringBuilder.Append($"Block {item.ItemName} was {item.action} here by {item.PlayerName} ({item.PlayerId}) at {item.Time} {item.TimeZone}");
                                user.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                            }
                        }
                        else
                        {
                            LocStringBuilder locStringBuilder = new LocStringBuilder();
                            locStringBuilder.Append("This block has not been placed");
                            user.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                        }
                        return;
                    }
                }
            }
        }

    }
}

