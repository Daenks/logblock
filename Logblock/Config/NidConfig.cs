﻿namespace NIDMod
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Nid
    {
        [JsonProperty("ModeratorsModule")]
        public ModeratorsModule ModeratorsModule { get; set; }

        [JsonProperty("GeneralSettings")]
        public GeneralSettings GeneralSettings { get; set; }

        [JsonProperty("MotdModule")]
        public MotdModule MotdModule { get; set; }

        [JsonProperty("RulesModule")]
        public RulesModule RulesModule { get; set; }

        [JsonProperty("ServerChangelogModule")]
        public ServerChangelogModule ServerChangelogModule { get; set; }

        [JsonProperty("HomeTeleportModule")]
        public HomeTeleportModule HomeTeleportModule { get; set; }

        [JsonProperty("IpLoggerModule")]
        public IpLoggerModule IpLoggerModule { get; set; }
    }

    public partial class GeneralSettings
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("InformationA")]
        public string InformationA { get; set; }

        [JsonProperty("ServerOwnerSteamID")]
        public string ServerOwnerSteamId { get; set; }

        [JsonProperty("RequireMintingRights")]
        public bool RequireMintingRights { get; set; }

        [JsonProperty("AllowMarriages")]
        public bool AllowMarriages { get; set; }

        [JsonProperty("FadeModName")]
        public bool FadeModName { get; set; }

        [JsonProperty("LogChat")]
        public bool LogChat { get; set; }
    }

    public partial class HomeTeleportModule
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("TimerEnabled")]
        public bool TimerEnabled { get; set; }

        [JsonProperty("TimerMinutes")]
        public long TimerMinutes { get; set; }

        [JsonProperty("CostCaloriesEnabled")]
        public bool CostCaloriesEnabled { get; set; }

        [JsonProperty("CostCaloriesAmount")]
        public long CostCaloriesAmount { get; set; }

        [JsonProperty("CostGlobalCurrencyEnabled")]
        public bool CostGlobalCurrencyEnabled { get; set; }

        [JsonProperty("CostGlobalCurrencyAmount")]
        public long CostGlobalCurrencyAmount { get; set; }
    }

    public partial class IpLoggerModule
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("InformationA")]
        public string InformationA { get; set; }

        [JsonProperty("InformationB")]
        public string InformationB { get; set; }

        [JsonProperty("ModuleEnabled")]
        public bool ModuleEnabled { get; set; }

        [JsonProperty("MakeCSVFile")]
        public bool MakeCsvFile { get; set; }
    }

    public partial class ModeratorsModule
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("Moderators")]
        public Moderator[] Moderators { get; set; }
    }

    public partial class Moderator
    {
        [JsonProperty("Identifier")]
        public string Identifier { get; set; }
    }

    public partial class MotdModule
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("isEnabled")]
        public bool IsEnabled { get; set; }

        [JsonProperty("showAsPopUp")]
        public bool ShowAsPopUp { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }

        [JsonProperty("WindowTitle")]
        public string WindowTitle { get; set; }

        [JsonProperty("showInChat")]
        public bool ShowInChat { get; set; }
    }

    public partial class RulesModule
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("ForceAcceptRules")]
        public bool ForceAcceptRules { get; set; }

        [JsonProperty("ForcePassPhrase")]
        public bool ForcePassPhrase { get; set; }

        [JsonProperty("PassPhrase")]
        public string PassPhrase { get; set; }

        [JsonProperty("PreventClaiming")]
        public bool PreventClaiming { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }

        [JsonProperty("ForceRulesPopUp")]
        public bool ForceRulesPopUp { get; set; }
    }

    public partial class ServerChangelogModule
    {
        [JsonProperty("$type")]
        public string Type { get; set; }

        [JsonProperty("FileName")]
        public string FileName { get; set; }
    }

    public partial class Nid
    {
        public static Nid FromJson(string json) => JsonConvert.DeserializeObject<Nid>(json, NIDMod.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Nid self) => JsonConvert.SerializeObject(self, NIDMod.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
