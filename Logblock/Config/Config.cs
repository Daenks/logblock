﻿using JsonConfigSaver;
using Newtonsoft.Json;


namespace Logblock.Config
{
    public sealed class LogBlockConfig : JsonEcoConfig
    {
        [JsonProperty]
        public bool LogToConsole = false;
        [JsonProperty]
        public bool LogBlockPlacement = true;
        [JsonProperty]
        public bool LogBlockPickup = true;
        [JsonProperty]
        public bool LogTreeFell = true;
        [JsonProperty]
        public int daysTillPrune = 5;
        [JsonProperty]
        public string PathToDB = null;

        //Nid Section
        [JsonProperty]
        public bool UseNidPermissions = false;
        [JsonProperty]
        public string PathToNidSettings = "Mods/NidToolbox/Config/Settings.json";
        //Commands for moderators
        [JsonProperty]
        public bool QueryBlock = true;

        public LogBlockConfig(string plugin, string name) : base(plugin, name)
        {
        }
    }
}
