﻿using System;

namespace Logblock.dataBaseRecord
{
    public class LoggedBlockPlacement
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public int PlayerId { get; set; }
        public string action { get; set; }
        public string ItemName { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }
        public DateTime Time { get; set; }
        public string TimeZone { get; set; }
    }
}